import Response from 'models/Response'

export const get = ({id}: {id: string}) =>
  Response.findByPk(id)

export const update = async ({id, data}: {id: string, data: any}) => {
  const response = await Response.findByPk(id)
  if (!response)
    return create({data})

  await response.update({
    data: Object.assign(response.data, data)
  })
  return response
}

export const create = ({data}: {data: any}) =>
  Response.create({data})

export const submit = async ({id}: {id: string}) => {
  const response = await Response.findByPk(id)
  if (!response)
    return undefined

  await response.update({submitted: true})
  return response
}
