import {Request, Response} from 'express'
import {get, update, create, submit} from './actions'
import * as Yup from 'yup'

const responseDataSchema = Yup.object().shape({
  data: Yup.object().required(),
})

const schemas = {
  update: responseDataSchema,
  create: responseDataSchema,
}

export default {
  get: async (req: Request, res: Response) => {
    res.json(await get({id: req.params.id}))
  },
  update: async (req: Request, res: Response) => {
    const {data} = schemas.update.validateSync(req.body)
    res.json(await update({id: req.params.id, data}))
  },
  create: async (req: Request, res: Response) => {
    const {data} = schemas.create.validateSync(req.body)
    const survey = await create({data})
    res.json({id: survey.id})
  },
  submit: async (req: Request, res: Response) => {
    if (await submit({id: req.params.id}))
      res.sendStatus(200)
    else
      res.status(422).json({message: 'Response not found', type: 'not_found'})
  }
}