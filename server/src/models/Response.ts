import { Model, DataTypes } from 'sequelize'
import db from 'config/sequelize'

export default class User extends Model {
  id!: string
  data!: string
  submitted!: boolean
  createdAt!: Date
  updatedAt!: Date
}

User.init({
  id: {
    type: DataTypes.UUIDV4,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  data: {
    type: DataTypes.JSONB,
    allowNull: false,
  },
  submitted: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  updatedAt: {
    type: DataTypes.DATE,
  },
  createdAt: {
    type: DataTypes.DATE,
  }
}, {
  tableName: 'responses',
  sequelize: db,
})
