import express from 'express'
import dotenv from 'dotenv'
import 'express-async-errors'

const main = async () => {
  dotenv.config()

  const app = express()
  const port = process.env.PORT

  app.listen(port, () =>
    console.log(`App listening at http://localhost:${port}`)
  )

  if (process.env.NODE_ENV === 'development') {
    const {cache} = require
    const persistentFiles: string[] = ['config/sequelize']
    const clearCache = (except: string[]) => {
      for (let key in cache)
        if (!except.includes(key) && key.indexOf('/node_modules/') === -1)
          delete cache[key]
    }
    app.use((req, res, next) => {
      clearCache(persistentFiles)
      const {router} = require('config/routes')
      router.handle(req, res, next)
    })
  } else {
    const {router} = require('config/routes')
    app.use((req, res, next) => {
      router.handle(req, res, next)
    })
  }
}

main()
