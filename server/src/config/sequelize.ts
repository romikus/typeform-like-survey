import { Sequelize } from 'sequelize'
import envConfigs from '../../database'

const config = envConfigs[process.env.NODE_ENV as 'development' | 'production']

export default new Sequelize(config.url as string)
