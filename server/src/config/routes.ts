import {Router, static as expressStatic} from 'express'
import {resolve} from 'path'
import cors from 'cors'
import bodyParser from 'body-parser'
import {handleError} from 'middleware/handleError'

import surveyResponse from 'app/Response/api'

export const router = Router()

router.use('*', cors())
router.use(bodyParser.json())

router.get('/api/v1/responses/:id', surveyResponse.get)
router.post('/api/v1/responses/:id', surveyResponse.update)
router.post('/api/v1/responses/:id/submit', surveyResponse.submit)
router.post('/api/v1/responses', surveyResponse.create)

router.use(expressStatic('../client/build'))
const frontendFile = resolve(__dirname, '..', '..', '..', 'client/build/index.html')
router.get('*', (req, res) => {
  res.sendFile(frontendFile)
})

router.use(handleError)
