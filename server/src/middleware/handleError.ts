import {ErrorRequestHandler} from 'express'

export const handleError: ErrorRequestHandler = (err, req, res, next) => {
  if (err.name === 'ValidationError') {
    res.status(422).send({error: err.message})
  } else if (err.name === 'MongoError') {
    if (err.code === 11000)
      res.status(422).send({message: 'Record not unique', type: 'record_not_unique'})
  } else {
    res.status(500).send({message: 'Something went wrong'})
  }

  next(err)
}
