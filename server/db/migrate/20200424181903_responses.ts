import {Migration} from 'rake-db'

export const change = (db: Migration, up: boolean) => {
  if (up)
    db.exec`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`
  else
    db.exec`DROP EXTENSION IF EXISTS "uuid-ossp"`

  db.createTable('responses', {id: false}, (t) => {
    t.column('id', 'uuid', {
      default: 'uuid_generate_v4()',
      primaryKey: true
    })
    t.column('data', 'jsonb', {null: false})
    t.boolean('submitted', {default: 'false'})
    t.timestamps()
  })
}
