export const routes = {
  api: {
    response: {
      get: (id: string) => `/api/v1/responses/${id}`,
      update: (id: string) => `/api/v1/responses/${id}`,
      submit: (id: string) => `/api/v1/responses/${id}/submit`,
      create: () => `/api/v1/responses`,
    }
  }
}