import {get, post} from 'utils/fetch'
import {routes} from 'config/routes'

export const getResponse = async () => {
  const id = localStorage.getItem('responseId')
  if (!id)
    return undefined

  return await get(routes.api.response.get(id))
}

export const createResponse = async (name: string, value: string) => {
  const {id} = await post(routes.api.response.create(), {data: {[name]: value}})
  localStorage.setItem('responseId', id)
}

export const updateResponse = async (name: string, value: string) => {
  const id = localStorage.getItem('responseId')
  if (!id)
    return await createResponse(name, value)

  const {id: newId} = await post(routes.api.response.update(id), {data: {[name]: value}})
  localStorage.setItem('responseId', newId)
}

export const submitResponse = async () => {
  const id = localStorage.getItem('responseId')
  if (!id)
    throw new Error('Can not submit response')

  return await post(routes.api.response.submit(id))
}
