import {ElementType, ButtonAction, SurveyJSON} from "types/survey"

const json: SurveyJSON = {
  success: 'Færdig! Al information er afsendt',
  errors: {
    required: 'Udfyld venligst dette'
  },
  elements: [
    {
      type: ElementType.radiogroup,
      name: 'firstQuesion',
      title: 'Hvilken periode ønsker du hjælp til?',
      required: true,
      options: [
        {
          value: '2019',
          hint: 'Task',
        },
        {
          value: '2020',
          hint: 'Task',
        }
      ]
    },
    {
      type: ElementType.radiogroup,
      name: 'secondQuestion',
      title: 'Hvad skal vi lave for din enkeltmandsvirksomhed/dit selskab?',
      required: true,
      options: [
        {
          value: 'All-inclusive pakken: Udarbejdelse og indberetning af årsregnskab, skatteregnskab for 2020 og moms, samt bogføring og bankafstemning',
          hint: 'Task',
        },
        {
          value: 'Årsafslutningspakken: Kun udarbejdelse og indberetning af årsregnskab, skatteregnskab for 2020, jeg sørger selv for bogføringen.',
          hint: 'Task',
        },
      ]
    },
    {
      type: ElementType.rate,
      title: 'Hvor mange bank kontier har du tilknyttet virksomheden',
      name: 'rate',
      rateMin: 0,
      rateMax: 10,
      rateStep: 1,
    },
    {
      type: 'custom',
      html: '<blockquote>If many revenue transactions, then ask whether the customer sometimes sells things abroad (meaning no VAT).</blockquote>',
      buttonsAlign: 'left',
      buttons: [
        {
          text: 'Fortsæt',
          action: ButtonAction.next,
          afterForDesktop: 'tryk <b>Enter ↵</b>'
        }
      ]
    },
    {
      type: 'custom',
      html: '<blockquote>Mangelliste</blockquote>',
      buttonsAlign: 'center',
      buttons: [
        {
          text: 'Indsend',
          action: ButtonAction.submit
        }
      ]
    },
  ]
}

export default json
