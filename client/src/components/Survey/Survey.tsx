import React from 'react'
import {getResponse, updateResponse, submitResponse} from "./actions"
import SurveyForm from 'components/SurveyForm'
import surveyData from "./surveyData"

export default () => {
  const [loaded, setLoaded] = React.useState(false)
  const [response, setResponse] = React.useState({})

  React.useEffect(() => {
    getResponse().then(response => {
      setLoaded(true)
      if (response)
        setResponse(response.data)
    })
  }, [])

  if (!loaded)
    return null

  return <SurveyForm data={surveyData} response={response} update={updateResponse} submit={submitResponse} />
}
