import React from 'react'
import style from './style.module.scss'
import Model from 'components/SurveyForm/Model'
import Font from 'components/SurveyForm/Font/Font'

export default ({model: {json: {success}}}: {model: Model}) =>
  <div className={style.page}><Font size='xl'>{success}</Font></div>
