import React from 'react'
import Model from './Model'
import {CustomData, ElementType, RadioGroupData, RateData, SurveyJSON} from 'types/survey'
import Swipe from "./Swipe/Swipe"
import {nextQuestion, prevQuestion} from "./actions"
import Screen from "./Screen/Screen"
import RadioGroup from "./Elements/RadioGroup/RadioGroup"
import Rate from "./Elements/Rate/Rate"
import Custom from "./Elements/Custom/Custom"
import SuccessPage from './SuccessPage/SuccessPage'
import {observer} from 'mobx-react'
import Pagination from './Pagination/Pagination'
import style from './style.module.scss'

const SurveyForm = observer(({model}: {model: Model}) => {
  if (model.state === 'submitted')
    return <SuccessPage model={model} />

  return <Swipe prev={() => prevQuestion(model)} next={() => nextQuestion(model)}>
    <React.Fragment>
      {model.json.elements.map((data, i) => {
        const {type} = data
        const number = i + 1

        return <Screen key={i} number={i} model={model}>
          <div className={style.container}>
            {type === ElementType.radiogroup && <RadioGroup number={number} model={model} data={data as RadioGroupData} />}
            {type === ElementType.rate && <Rate number={number} model={model} data={data as RateData} />}
            {type === ElementType.custom && <Custom model={model} data={data as CustomData}/>}
          </div>
        </Screen>
      })}
    </React.Fragment>
    <Pagination model={model} />
  </Swipe>
})

type Props = {
  data: SurveyJSON,
  response: any
  update: (name: string, value: any) => any
  submit: () => any
}

export default ({data, response, update, submit}: Props) =>
  <SurveyForm model={new Model({data, response, update, submit})} />
