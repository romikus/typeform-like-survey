import React from 'react'
import Font from 'components/SurveyForm/Font/Font'
import style from './style.module.scss'
import Error from "../Error/Error";
import Model from "../Model";

type Props = {
  children: React.ReactNode
  number: number
  title: string
  model: Model
  name: string
  required?: boolean
}

export default ({children, number, title, model, name, required}: Props) => {
  return <React.Fragment>
    <div className={style.questionHeader}>
      <div className={style.counter}>
        <div className={style.counterValue}>
          <Font size='s'>{number}</Font>
        </div>
        <svg className={style.counterArrow} height="10" width="11">
          <path d="M7.586 5L4.293 1.707 5.707.293 10.414 5 5.707 9.707 4.293 8.293z"/>
          <path d="M8 4v2H0V4z"/>
        </svg>
      </div>
      <div className={style.question}>
        <Font size='xl'>{title}</Font>
        {required && <Font size='xl' className={style.requiredAsterisk}>*</Font>}
      </div>
    </div>
    <div tabIndex={-1} className={style.answer}>
      {children}
    </div>
    <Error model={model} name={name} />
  </React.Fragment>
}
