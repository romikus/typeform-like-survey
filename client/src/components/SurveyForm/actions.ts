import Model from './Model'

export const nextQuestion = (model: Model) => {
  const number = model.currentQuestion + 1
  if (number < model.json.elements.length)
    model.setCurrentQuestion(number)
}

export const prevQuestion = (model: Model) => {
  const number = model.currentQuestion - 1
  if (number >= 0)
    model.setCurrentQuestion(number)
}

const animationMs = 250

export const setValue = (model: Model, name: string, value: any) => {
  model.update(name, value)
  model.setValue(name, value)
  const {currentQuestion} = model
  setTimeout(() => {
    if (model.currentQuestion === currentQuestion)
      nextQuestion(model)
  }, animationMs)
}

export const submit = async (model: Model) => {
  const firstErrorQuestionNumber = model.validate()
  if (firstErrorQuestionNumber !== undefined)
    return model.setCurrentQuestion(firstErrorQuestionNumber)

  model.state = 'submitting'
  await model.submit()
  model.state = 'submitted'
}
