import React from 'react'
import cn from 'classnames'
import style from './style.module.scss'

type Size = 'xl' | 'l' | 'm' | 's' | 'xs'

export default (
  {size, className, ...props}: any & {size: Size, className?: string}
) =>
  <span className={cn(style[size], className)} {...props} />
