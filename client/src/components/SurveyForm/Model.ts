import {action, observable} from 'mobx'
import {Question, SurveyJSON} from 'types/survey'

type UpdateFunction = (name: string, value: any) => any
type SubmitFunction = () => any

type Props = {
  data: SurveyJSON
  response: any
  update: UpdateFunction
  submit: SubmitFunction
}

export default class FormModel {
  @observable json: SurveyJSON
  @observable currentQuestion = 0
  @observable prevQuestion = 0
  @observable values: any = {}
  @observable errors: {[key: string]: undefined | string} = {}
  @observable state: 'running' | 'submitting' | 'submitted' = 'running'
  update: UpdateFunction
  submit: SubmitFunction

  constructor({data, response, update, submit}: Props) {
    this.json = data
    for (let key in response)
      this.values[key] = response[key]
    this.update = update
    this.submit = submit
  }

  @action setCurrentQuestion(number: number) {
    this.prevQuestion = this.currentQuestion
    this.currentQuestion = number
  }

  @action setValue(name: string, value: any) {
    this.values[name] = value
    const question = this.json.elements.find(element =>
      (element as Question).name === name
    )
    this.validateQuestion(question as Question)
  }

  @action validateQuestion(question: Question) {
    const {name} = question
    if (!name) return

    if (question.required && !this.values[name])
      this.errors[name] = this.json.errors.required
    else
      this.errors[name] = undefined
  }

  @action validate() {
    let firstErrorQuestionNumber: undefined | number
    this.json.elements.forEach((element, i) => {
      const {name} = element as Question
      if (name)
        this.validateQuestion(element as Question)

      if (firstErrorQuestionNumber === undefined && this.errors[name])
        firstErrorQuestionNumber = i
    })
    return firstErrorQuestionNumber
  }
}