import React from 'react'
import Model from 'components/SurveyForm/Model'
import {observer} from 'mobx-react'
import style from "./style.module.scss";

type Props = {
  model: Model
  name?: string
}

export default observer(({model, name}: Props) => {
  const error = name && model.errors[name]
  return <div className={style.errorSpace}>
    {error && <div className={style.error}>{error}</div>}
  </div>
})
