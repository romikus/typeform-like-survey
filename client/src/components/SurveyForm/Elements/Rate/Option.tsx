import React from 'react'
import {observer} from 'mobx-react'
import Font from 'components/SurveyForm/Font/Font'
import style from './style.module.scss'
import Model from 'components/SurveyForm/Model'
import {setValue} from 'components/SurveyForm/actions'

type Props = {
  number: number
  model: Model
  name: string
}

export default observer(({number, model, name}: Props) => {
  const handleChange = ({target: {value}}: React.ChangeEvent<HTMLInputElement>) =>
    setValue(model, name, parseInt(value))

  return <label className={style.optionWrap}>
    <input
      type='radio'
      className={style.input}
      name={name}
      value={number}
      checked={number === model.values[name]}
      onChange={handleChange}
    />
    <Font size='m' className={style.option}>{number}</Font>
  </label>
})
