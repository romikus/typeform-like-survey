import React from 'react'
import {CustomData} from 'types/survey'
import style from './style.module.scss'
import Font from 'components/SurveyForm/Font/Font'
import Model from 'components/SurveyForm/Model'
import {nextQuestion, submit} from 'components/SurveyForm/actions'

type Props = {
  data: CustomData
  model: Model
}

const actions = {
  next(model: Model) {
    nextQuestion(model)
  },
  submit(model: Model) {
    submit(model)
  }
}

export default ({model, data: {html, buttonsAlign = 'center', buttons}}: Props) => {
  return <div className={style.wrap}>
    {html && <Font size='xl' className={style.content} dangerouslySetInnerHTML={{__html: html}}/>}
    {buttons &&
      <div
        className={style.buttons}
        style={buttonsAlign ? {justifyContent: buttonsAlign} : undefined}
      >
        {buttons.map((button, i) =>
          <React.Fragment key={i}>
            <button
              key={i}
              className={style.button}
              onClick={() => actions[button.action](model)}
            >
              <Font size='l'>{button.text}</Font>
            </button>
            {button.afterForDesktop &&
              <div className={style.buttonDesktopHint} dangerouslySetInnerHTML={{__html: button.afterForDesktop}}/>
            }
          </React.Fragment>
        )}
      </div>
    }
  </div>
}
