import React from 'react'
import style from './style.module.scss'
import {RadioGroupData} from 'types/survey'
import Fieldset from 'components/SurveyForm/Fieldset/Fieldset'
import Model from 'components/SurveyForm/Model'
import Option from './Option'

type Props = {
  number: number
  model: Model
  data: RadioGroupData
}

export default ({number, model, data: {title, required, name, options}}: Props) => {
  return <Fieldset number={number} title={title} model={model} name={name} required={required}>
    <div className={style.wrap}>
      {options.map((option, i) =>
        <Option key={i} number={i} model={model} name={name} option={option} />
      )}
    </div>
  </Fieldset>
}
