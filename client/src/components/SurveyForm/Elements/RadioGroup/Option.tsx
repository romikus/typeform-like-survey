import React from 'react'
import Model from 'components/SurveyForm/Model'
import {observer} from 'mobx-react'
import {setValue} from 'components/SurveyForm/actions'
import style from './style.module.scss'
import Font from 'components/SurveyForm/Font/Font'

type Props = {
  number: number
  model: Model
  name: string
  option: {value: string, text?: string, hint?: string}
}

const ACode = 'A'.charCodeAt(0)

export default observer(({number, model, name, option: {value, text = value, hint}}: Props) => {
  const handleChange = ({target: {value}}: React.ChangeEvent<HTMLInputElement>) =>
    setValue(model, name, value)

  return <label className={style.choiceWrap}>
    <input
      name={name}
      value={value}
      checked={value === model.values[name]}
      onChange={handleChange}
      className={style.input}
      type='radio'
    />
    <div className={style.choice}>
      <div className={style.key}>
        <div className={style.keyContent}>
          {hint && <Font size='xs' className={style.keyHint}>{hint}</Font>}
          <Font size='xs' className={style.keyLetter}>{String.fromCharCode(ACode + number)}</Font>
        </div>
      </div>
      <Font size='l' className={style.choiceText}>{text}</Font>
      <div className={style.check}>
        <svg className={style.checkIcon} height="13" width="16">
          <path d="M14.293.293l1.414 1.414L5 12.414.293 7.707l1.414-1.414L5 9.586z"/>
        </svg>
      </div>
    </div>
  </label>
})
