export enum ElementType {
  radiogroup = 'radiogroup',
  rate = 'rate',
  custom = 'custom'
}

export enum ButtonAction {
  next = 'next',
  submit = 'submit'
}

export interface SurveyElement {
  type: ElementType,
  name: string
  title: string
  required?: boolean
}

export interface RadioGroupData extends SurveyElement {
  options: {
    value: string
    text?: string
    hint?: string
  }[]
}

export interface RateData extends SurveyElement {
  rateMin: number
  rateMax: number
  rateStep?: number
}

export interface CustomData {
  type: 'custom'
  html?: string
  buttonsAlign?: string
  buttons?: {
    text: string
    action: ButtonAction
    afterForDesktop?: string
  }[]
}

export type Question =
  RadioGroupData |
  RateData

export type AnyElement =
  Question |
  CustomData

export interface SurveyJSON {
  success: string
  errors: {
    required: string
  }
  elements: AnyElement[]
}